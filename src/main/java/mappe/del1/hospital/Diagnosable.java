package mappe.del1.hospital;
/**
 * Class for the diagnosis of patient
 * @author Gaute Degré Lorentsen
 * @version 1.0.0
 */
public interface Diagnosable{
    void setDiagnosis(String diagnose);

}
