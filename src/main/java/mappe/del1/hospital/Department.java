package mappe.del1.hospital;

import mappe.del1.hospital.exception.RemoveException;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

/**
 * Department class.
 * @author Gaute Degré Lorentsen
 * @version 1.0.0
 */
public class Department{
    String departmentName;
    // Makes a list out of Employees
    private ArrayList<Employee> employees = new ArrayList<Employee>();
    // Makes a list out of patients
    private ArrayList<Patient> patients = new ArrayList<Patient>();

    public Department(String departmentName) {
        this.departmentName = departmentName;
    }


    public String getDepartmentName() {
        return departmentName;
    }

    public void setDepartmentName(String departmentName) {
        this.departmentName = departmentName;
    }

    public List<Employee> getEmployees(){
        return employees;
    }

    public void addEmployee(Employee newEmployee){
            employees.add(newEmployee);

    }

    public List<Patient> getPatients(){
        return patients;
    }

    /**
     * Method for adding a new patient.
     * @param newPatient
     */
    public void addPatient(Patient newPatient){
        if (!this.patients.contains(newPatient)){
            patients.add(newPatient);
        }

    }

    @Override
    public int hashCode() {
        return Objects.hash(departmentName, employees, patients);
    }

    /**
     * A method to remove a person from either the Patient class or the Employee class.
     * If the person is not in the list a RemoveEcception error message is thrown.
     * @param removePerson
     * @throws RemoveException
     */
    public void remove(Person removePerson) throws RemoveException {
        if (removePerson instanceof Patient) {
            if (patients.contains(removePerson)){
                patients.remove(removePerson);
            }else {
                throw new RemoveException();
            }
        } else if (removePerson instanceof Employee) {
            if (employees.contains(removePerson)){
                employees.remove(removePerson);
            }else {
                throw new RemoveException();
            }
        }



    }



    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Department that = (Department) o;
        return Objects.equals(departmentName, that.departmentName) &&
                Objects.equals(employees, that.employees) &&
                Objects.equals(patients, that.patients);
    }

    @Override
    public String toString() {
        return "Department{" +
                "departmentName='" + departmentName + '\'' +
                ", employees=" + employees +
                ", patients=" + patients +
                '}';
    }
}

