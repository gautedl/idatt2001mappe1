package mappe.del1.hospital;

import java.util.ArrayList;
/**
 * Class for hospitals
 * @author Gaute Degré Lorentsen
 * @version 1.0.0
 */
public class Hospital {

    private final String hospitalName;

    // creates a list with departments.
    private ArrayList<Department> departments = new ArrayList<Department>();

    public Hospital(String hospitalName) {
        this.hospitalName = hospitalName;
    }

    public String getHospitalName() {
        return hospitalName;
    }

    public ArrayList<Department> getDepartments() {
        return departments;
    }

    //adds new department to list of Department
    public void addDepartment(Department newDepartment){
        if(!(newDepartment instanceof Department)){ //Checks if department is already in list
            departments.add(newDepartment);
        }
        else {
            System.out.println("Department already registered.");
        }

    }

    @Override
    public String toString() {
        return "Hospital{" +
                "hospitalName='" + hospitalName + '\'' +
                ", departments=" + departments +
                '}';
    }
}
