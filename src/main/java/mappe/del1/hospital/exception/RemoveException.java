package mappe.del1.hospital.exception;
/**
 * Class for the remove exception error message
 * @author Gaute Degré Lorentsen
 * @version 1.0.0
 */
public class RemoveException extends Exception {
    public static final long serialVersionUID = 1L;

    /**
     * Error message that is displayed if a person is not in the register the program tries to remove it from
     */
    public RemoveException() {
        super("Person not in register.");
    }
}

