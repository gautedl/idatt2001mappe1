package mappe.del1.hospital;


import mappe.del1.hospital.exception.RemoveException;
/**
 * Main class for my app
 * @author Gaute Degré Lorentsen
 * @version 1.0.0
 */
public class HospitalClient {
    public static void main(String[] args) {


        Hospital testHospital = new Hospital("Hospital 1");
        HospitalTestData.fillRegisterWithTestData(testHospital);

        Patient testPatient = new Patient("Testy", "Testsen", "1210510");
        Department testDepartment = testHospital.getDepartments().get(0);
        Employee employee = testDepartment.getEmployees().get(0); //Employee that is first in the list
        Patient testPatient2= new Patient("Ulrik", "Smål", "");

        try {
            //Removes an employee that is in the list.
            testDepartment.remove(employee);
        } catch (RemoveException e){
            System.out.println("Employee could not be removed: " + e.getMessage());
        } try {
            //Tries to remove an employee that is not in the list.
            testDepartment.remove(testPatient);
        } catch (RemoveException e){
            System.out.println("Employee could not be removed: " + e.getMessage());



    }


}
}

