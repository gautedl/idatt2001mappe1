package mappe.del1.hospital;
/**
 * Patient class.
 * @author Gaute Degré Lorentsen
 * @version 1.0.0
 */

public class Patient extends Person implements Diagnosable{
    String diagnosis = "";

    /**
     *  Constructor of patient class.
     * @param firstName
     * @param lastName
     * @param socialSecurityNumber
     */
    protected Patient(String firstName, String lastName, String socialSecurityNumber) {
        super(firstName, lastName, socialSecurityNumber);
    }

    String getDiagnose(){
        return diagnosis;
    }

    /**
     * Sets diagnosis to patient
     * @param diagnosisPatient
     */
    @Override
    public void setDiagnosis(String diagnosisPatient) {
        diagnosis = diagnosisPatient;
    }

    protected String getDiagnosis() {
        return diagnosis;
    }
}
