package mappe.del1.hospital.healthpersonal.doctor;

import mappe.del1.hospital.Patient;
/**
 * Class for surgeon
 * @author Gaute Degré Lorentsen
 * @version 1.0.0
 */
public class Surgeon extends Doctor{

    public Surgeon(String firstName, String lastName, String socialSecurityNumber) {
        super(firstName, lastName, socialSecurityNumber);
    }

    /**
     * A Surgeon sets the diagnosis of a patient.
     * @param p
     * @param diagnose
     */
    @Override
    public void setDiagnosis(Patient p, String diagnose) {
        p.setDiagnosis(diagnose);
    }
}
