package mappe.del1.hospital.healthpersonal.doctor;

import mappe.del1.hospital.Patient;
/**
 * Class for General Practitioner
 * @author Gaute Degré Lorentsen
 * @version 1.0.0
 */
public class GeneralPractitioner extends Doctor{
    public GeneralPractitioner(String firstName, String lastName, String socialSecurityNumber) {
        super(firstName, lastName, socialSecurityNumber);
    }

    /**
     * A General Practitioner sets the diagnosis of a patient.
     * @param p
     * @param diagnose
     */
    public void setDiagnosis(Patient p, String diagnose) {
        super.setDiagnosis(p, diagnose);
    }
}