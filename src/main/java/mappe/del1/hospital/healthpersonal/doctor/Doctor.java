package mappe.del1.hospital.healthpersonal.doctor;

import mappe.del1.hospital.Employee;
import mappe.del1.hospital.Patient;
/**
 * Class for doctor
 * @author Gaute Degré Lorentsen
 * @version 1.0.0
 */
abstract class Doctor extends Employee {

    protected Doctor(String firstName, String lastName, String socialSecurityNumber) {
        super(firstName, lastName, socialSecurityNumber);
    }

    /**
     * A doctor sets the diagnosis of a patient.
     * @param p
     * @param diagnose
     */
    public void setDiagnosis(Patient p, String diagnose){
        p.setDiagnosis(diagnose);
    }

}

