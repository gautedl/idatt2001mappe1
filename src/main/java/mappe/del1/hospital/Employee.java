package mappe.del1.hospital;
/**
 * Class for employee
 * @author Gaute Degré Lorentsen
 * @version 1.0.0
 */
public class Employee extends Person{
    public Employee(String firstName, String lastName, String socialSecurityNumber) {
        super(firstName, lastName, socialSecurityNumber);
    }

    @Override
    public String toString() {
        return super.toString();
    }
}
