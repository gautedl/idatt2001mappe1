package mappe.del1.hospital;

import mappe.del1.hospital.exception.RemoveException;
import org.junit.jupiter.api.*;


public class departmentRemoveTest {
    Department testDepartment;
    Patient p;
    Patient p1;
    Employee e1;
    Employee e2;

    @Nested
    class RemoveTests {
        @Test
        public void throwArgumentWhenEmployeeNotinList() throws RemoveException {
            //Makes a test department, test patients and test employees.
            //Ads one of the patients and one of the employees to the department.
            testDepartment = new Department("Test Department");
            p = new Patient("Tor", "Torsen", "100");
            p1 = new Patient("Trine", "Trinesen", "120");
            e1 = new Employee("Tommy", "Tomsen", "200");
            e2 = new Employee("Truls", "Trulsen", "300");

            //Tests if we can remove an employee not in the list. Throws RemoveException error message if person not in list.
            testDepartment.addEmployee(e1);
            testDepartment.addPatient(p);
            Assertions.assertThrows(RemoveException.class, () -> {
                testDepartment.remove(e2);
            });
        }

        @Test
        public void testsTheSizeofTheListEmployeeAfterPersonisRemoved() throws RemoveException{
            //Makes a test department, test patients and test employees.
            //Ads one of the patients and one of the employees to the department.
            testDepartment = new Department("Test Department");
            p = new Patient("Tor", "Torsen", "100");
            p1 = new Patient("Trine", "Trinesen", "120");
            e1 = new Employee("Tommy", "Tomsen", "200");
            e2 = new Employee("Truls", "Trulsen", "300");

            //Test if we can remove an employee from the list. Tests if the size of the list is one less after remove function.
            testDepartment.addEmployee(e1);
            testDepartment.addPatient(p);
            int sizeList = testDepartment.getEmployees().size();
            testDepartment.remove(e1);
            Assertions.assertEquals(sizeList - 1, testDepartment.getEmployees().size());

        }

        @Test
        public void throwArgumentWhenPatientNotinList() throws RemoveException {
            //Makes a test department, test patients and test employees.
            //Ads one of the patients and one of the employees to the department.
            testDepartment = new Department("Test Department");
            p = new Patient("Tor", "Torsen", "100");
            p1 = new Patient("Trine", "Trinesen", "120");
            e1 = new Employee("Tommy", "Tomsen", "200");
            e2 = new Employee("Truls", "Trulsen", "300");

            //Tests if we can remove an patient not in the list. Throws RemoveException error message if person not in list.
            testDepartment.addEmployee(e1);
            testDepartment.addPatient(p);
            Assertions.assertThrows(RemoveException.class, () -> {
                testDepartment.remove(p1);
            });
        }
        @Test
        public void testsTheSizeofTheListPatientAfterPersonisRemoved() throws RemoveException{
            //Makes a test department, test patients and test employees.
            //Ads one of the patients and one of the employees to the department.
            testDepartment = new Department("Test Department");
            p = new Patient("Tor", "Torsen", "100");
            p1 = new Patient("Trine", "Trinesen", "120");
            e1 = new Employee("Tommy", "Tomsen", "200");
            e2 = new Employee("Truls", "Trulsen", "300");

            //Test if we can remove an patient from the list. Tests if the size of the list is one less after remove function.
            testDepartment.addEmployee(e1);
            testDepartment.addPatient(p);
            int sizeList = testDepartment.getPatients().size();
            testDepartment.remove(p);
            Assertions.assertEquals(sizeList - 1, testDepartment.getPatients().size());

        }



    }
}